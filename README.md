# VideoJuegos

Para instalar el proyecto necesitan de 2 llaves, son ofrecidas por rapidapi y rawg.

Instalacion de manera normal con el comando correspondiente a

`npm install`

Proyecto hecho en Angular 11

Capturas:

<img src="https://gitlab.com/soy.pepe/video_juegos/-/raw/master/video-juegos1.JPG" width="500" height="300">
<img src="https://gitlab.com/soy.pepe/video_juegos/-/raw/master/video-juegos2.JPG" width="500" height="300">
<img src="https://gitlab.com/soy.pepe/video_juegos/-/raw/master/video-juegos3.JPG" width="500" height="300">
